/**
 * Class which takes undefined number of objects and converts them to concatenated <code>String</code>.
 *
 * @author Андрiй
 */
public class StringUtils {

    /**
     * Object storage.
     */
    private Object[] objects = null;

    /**
     * Conctructor with undefined numbers of <code>Object</code>'s.
     *
     * @param obj
     */
    public StringUtils(Object... obj) {
        this.objects = obj;
    }

    /**
     * Returns concatenated <code>String</code>.
     *
     * @return <code>String</code>
     */
    public String getAsString() {
        if (objects == null || objects.length == 0) return "";
        StringBuilder stringBuilder = new StringBuilder();
        for (Object _object : objects) {
            stringBuilder.append(_object.toString()).append(" ");
        }
        return stringBuilder.toString();
    }

}
