import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@FunctionalInterface
interface Doable {
    void doAction();
}

public class Main {

    public static final Logger logger = LogManager.getLogger(Main.class);

    /**
     * Data for tasks.
     * <p>
     * / /**
     * For task6 and task12.
     */
    private int wordLength = 6;
    /**
     * For task9.
     */
    private int character = 'п';
    /**
     * For task10.
     */
    private List<String> words = Arrays.asList("знайти", "вивести", "порядку");
    /**
     * For task 11.
     */
    private char wordBegins = 'к';
    private char wordEnds = 'і';

    /**
     * For task16.
     */
    private String substring = "Я_ТІПА_ПІДРЯДОК:DDDD";

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("locale", Locale.US);

    public static void main(String[] args) {
        Main main = new Main();

        Map<String, Doable> menuMap = new LinkedHashMap();
        menuMap.put("1", main::changeLocale);
        menuMap.put("2", main::taskWithStringUtils);
        menuMap.put("3", main::taskWithBigText);
        main.printMenu();
        try (Scanner scanner = new Scanner(System.in)) {
            while (scanner.hasNext()) {

                String input = scanner.nextLine();
                menuMap.get(input).doAction();
                main.printMenu();
            }

        }

    }

    /**
     * Changing locale en_US -> kr_KO. Another invocation swaps locales back.
     */
    private void changeLocale() {
        if (resourceBundle.getLocale().equals(Locale.US)) {
            resourceBundle = ResourceBundle.getBundle("locale", Locale.KOREA);
        } else {
            resourceBundle = ResourceBundle.getBundle("locale", Locale.US);
        }
    }

    /**
     * Prints to the console users menu.
     */
    void printMenu() {
        String menuFormat = "%s:\n" +
                "1 - %s.\n2 - %s.\n3 - %s.";
        String format = String.format(menuFormat,
                resourceBundle.getString("menu.help"),
                resourceBundle.getString("menu.change.language"),
                resourceBundle.getString("menu.task"),
                resourceBundle.getString("menu.big.task"));
        System.out.println(format);
    }

    /**
     * <code>StringUtils</code> usage.
     */
    void taskWithStringUtils() {
        StringUtils stringUtils = new StringUtils(Main.class, String.class, "Ptah", 999L);

        System.out.println(stringUtils.getAsString());
    }

    void taskWithBigText() {
        String text = new String();
        StringBuilder builder = new StringBuilder();
        try (FileReader fileReader = new FileReader(new File("src/text.txt"))) {
            int k;
            while ((k = fileReader.read()) != -1) {
                builder.append((char) k);
            }
            text = builder.toString();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        Method[] methods = this.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().matches("task[\\d]+")) {
                System.out.println("Invoking method: " + method);
                try {
                    method.invoke(this, text);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    logger.warn("InvocationTargetException: " + e.getMessage());
                }
                System.out.println();
            }
        }
    }

    /**
     * Calculates max count of sentences which contains same words.
     *
     * @param text string to handle.
     */
    void task1(String text) {
        System.out.println("Знайти найбільшу кількість речень тексту, в яких є однакові слова.\n");
        String[] sentences = text.split("[.!?]+");
        int countOfSentencesWithSameWords = 0;
        for (String s : sentences) {
            String[] words = s.split("[ ,-]+");
            boolean flag = false;
            for (int i = 0; i < words.length; i++) {
                for (int j = i + 1; j < words.length - 1; j++) {
                    if (words[i].equals(words[j].trim())) {
                        System.out.println("_" + words[i] + "_ :" + words[j] + ": ");
                        flag = true;
                    }
                }
            }
            if (flag) {
                flag = false;
                countOfSentencesWithSameWords++;
            }
        }
        System.out.println("Max count of sentences with same words is:" + countOfSentencesWithSameWords);
    }

    /**
     * Prints out to <code>System.out</code> the sentences from <code>text</code> limited by <i>. ! ?</i> by ascending order.
     *
     * @param text string to handle
     */
    void task2(String text) {
        System.out.println("Вивести всі речення заданого тексту у порядку зростання кількості слів у\n" +
                "кожному з них.\n");
        String[] sentences = text.split("[.!?]+");
        String pattern = "[ ,-]+";
        Set<String> set = new TreeSet<>((u, i) -> u.split(pattern).length - i.split(pattern).length);
        for (String sentence : sentences) {
            set.add(sentence);
        }
        set.forEach(System.out::println);
    }

    /**
     * Prints out the words from first sentence which are not duplicates in rest of sentences.
     *
     * @param text string to handle
     */
    void task3(String text) {
        System.out.println("Знайти таке слово у першому реченні, якого немає ні в одному з інших\n" +
                "речень.\n");
        Pattern pattern = Pattern.compile("\\S[^.]*[.!?]");
        Matcher matcher = pattern.matcher(text);
        boolean b = matcher.find();
        String firstSentence = null;
        String restOfText = null;
        if (b) {
            firstSentence = text.substring(matcher.start(), matcher.end());
            restOfText = text.substring(matcher.end())
                    .toLowerCase();
        }
        String[] wordsInFirstSentence = firstSentence.split("[ ./?!,():]");
        for (String s : wordsInFirstSentence) {
            if (!restOfText.contains(s.toLowerCase())) {
                System.out.println("\"" + s + "\" is a unique word.");
            }
        }
    }

    /**
     * In all questions seeks words with set length.
     *
     * @param text string to handle
     */
    void task4(String text) {
        System.out.println("У всіх запитальних реченнях тексту знайти і надрукувати без повторів\n" +
                "слова заданої довжини.\n");
        Pattern pattern = Pattern.compile("[^.]*[?]", Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(text);
        String sentence = null;
        while (matcher.find()) {
            sentence = text.substring(matcher.start(), matcher.end())
                    .trim();
            System.out.println("Question: " + sentence);
            Matcher _mat = Pattern.compile("\\b[А-я]{" + wordLength + "}\\b").matcher(sentence);
            while (_mat.find()) {
                System.out.println(sentence.substring(_mat.start(), _mat.end()));
            }
        }
    }

    void task5(String text) {
        System.out.println("У кожному реченні тексту поміняти місцями перше слово, що починається\n" +
                "на голосну букву з найдовшим словом.\n");
        String[] sentences = text.split("[.!?]+");
        for (String sentence : sentences) {
            String[] words = sentence.split("[ ./?!,():]");
            Optional<String> max = Stream.of(words).max(Comparator.comparingInt(String::length));
            int maxIndex = 0;
            for (int i = 0; i < words.length; i++) {
                if (max.toString().equals(words[i])) {
                    maxIndex = i;
                }
            }

            for (int i = 0; i < words.length; i++) {
                if (words[i].matches("[АаОоУуЕеІіИиЯяЮюЄєЇї]") && i != maxIndex) {
                    String buffer = words[maxIndex];
                    words[maxIndex] = words[i];
                    words[i] = buffer;
                    break;
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (String word : words) {
                stringBuilder.append(word).append(" ");
            }
            System.out.println(stringBuilder.toString());
        }
    }

    void task6(String text) {
        System.out.println("Надрукувати слова тексту в алфавітному порядку по першій букві. Слова,\n" +
                "що починаються з нової букви, друкувати з абзацного відступу.\n");
        String[] split = text.replaceAll("^[.,\\s]+", "").split("[.,\\s]+");
        Arrays.sort(split, Comparator.comparingInt(u -> u.toLowerCase().charAt(0)));
        char previousChar = 0;
        for (String s : split) {
            if (previousChar != s.charAt(0)) {
                System.out.print("\t");
            }
            System.out.println(s);
        }
    }

    void task7(String text) {
        System.out.println("Відсортувати слова тексту за зростанням відсотку голосних букв\n" +
                "(співвідношення кількості голосних до загальної кількості букв у слові).\n");
        String[] split = text.replaceAll("^[.,\\s():]+", "").split("[.,\\s():]+");
        Pattern pattern = Pattern.compile("[аоуеиіїюєя]");
        Arrays.sort(split, (o1, o2) -> {
            Matcher m1 = pattern.matcher(o1.toLowerCase());
            Matcher m2 = pattern.matcher(o2.toLowerCase());

            int vowelInFirst = 0;
            int vowelInSecond = 0;
            while (m1.find()) {
                vowelInFirst++;
            }
            while (m2.find()) {
                vowelInSecond++;
            }
            return (int) ((double) vowelInFirst / o2.length() * 10000 - (double) vowelInSecond / o1.length() * 10000);
        });
        for (String s : split) {
            System.out.println(s);
        }
    }

    void task8(String text) {
        System.out.println("Слова тексту, що починаються з голосних букв, відсортувати в\n" +
                "алфавітному порядку по першій приголосній букві слова.\n");
        String[] split = text.replaceAll("^[.,\\s():]+", "").split("[.,\\s():]+");
        List<String> list = new ArrayList<>();
        for (String s : split) {
            if (s.matches("^[аоуиеіїюєя]")) {
                list.add(s);
            }
        }
        //TODO: transitive
//        Arrays.sort(split, new Comparator<String>() {
//            Pattern pattern = Pattern.compile("[йцкншщзхфвпрлджчсмтьбю]");
//            @Override
//            public int compare(String o1, String o2) {
//                String _o1 = o1.toLowerCase();
//                String _o2 = o2.toLowerCase();
//                Matcher m1 = pattern.matcher(_o1);
//                Matcher m2 = pattern.matcher(_o2);
//                if(m1.find() && m2.find()) {
//                    int compare = _o1.charAt(m1.start()) - _o2.charAt(m2.start());
//                    System.out.println(compare);
//                    return compare == 0 ? 0 : (compare > 0 ? 1 : -1);
//                }
//                return 1;
//            }
//        });

        for (String s : split) {
            System.out.println(s);
        }
    }

    /**
     * Sorts and prints words which has a specified letter in ascending order.
     * Words which has same count of this letter are sorted in alphabet order.
     *
     * @param text string to handle.
     */
    void task9(String text) {
        System.out.println("Всі слова тексту відсортувати за зростанням кількості заданої букви у\n" +
                "слові. Слова з однаковою кількістю розмістити у алфавітному порядку.\n");
        Pattern pattern = Pattern.compile(String.format("[%c]", character));

        String[] split = text.replaceAll("^[.,\\s():]+", "").split("[.,\\s():]+");
        List<String> list = Arrays.asList(split);

        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String _o1 = o1.toLowerCase();
                String _o2 = o2.toLowerCase();
                Matcher matcher1 = pattern.matcher(_o1);
                Matcher matcher2 = pattern.matcher(_o2);

                int count1 = 0;
                int count2 = 0;
                while (matcher1.find()) {
                    count1++;
                }
                while (matcher2.find()) {
                    count2++;
                }
                if (count1 == count2) {
                    return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
                }
                return count1 - count2;
            }
        });

        list.forEach(System.out::println);
    }

    /**
     * Sorts and prints words from <code>words</code> list by count of entry in general text.
     *
     * @param text general text.
     */
    void task10(String text) {
        System.out.println("Є текст і список слів. Для кожного слова з заданого списку знайти, скільки\n" +
                "разів воно зустрічається у тексті, і відсортувати слова за\n" +
                "спаданням загальної кількості входжень.\n");
        Map<String, Integer> wordsAndCount = new HashMap<>(words.size());
        words.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                Pattern pattern = Pattern.compile(s);
                Matcher matcher = pattern.matcher(text.toLowerCase());
                int count = 0;
                while (matcher.find()) {
                    count++;
                }
                wordsAndCount.put(s, count);
            }
        });

        words.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return wordsAndCount.get(o1) - wordsAndCount.get(o2);
            }
        });
        words.forEach(System.out::println);
    }

    /**
     * Removes the biggest sequence from <code>wordBegins</code> to <code>wordEnds</code> in each sentence.
     *
     * @param text string to handle.
     */
    void task11(String text) {
        System.out.println("У кожному реченні тексту видалити підрядок максимальної довжини, що\n" +
                "починається і закінчується заданими символами.\n");
        String[] sentences = text.split("[.!?]+");
        StringBuilder stringBuilder = new StringBuilder();
        Pattern pattern = Pattern.compile(wordBegins + ".+" + wordEnds);

        for (String sentence : sentences) {
            Matcher matcher = pattern.matcher(sentence);
            String newSenctence = sentence;
            if (matcher.find()) {
                newSenctence = sentence.substring(0, matcher.start()) + sentence.substring(matcher.end());
            }
            stringBuilder.append(newSenctence);
        }
        System.out.println(stringBuilder.toString());
    }

    /**
     * Removes from <code>text</code> all words which starts with consonant letter and length <code>wordLength</code>.
     *
     * @param text string to handle.
     */
    void task12(String text) {
        System.out.println("З тексту видалити всі слова заданої довжини, що починаються на\n" +
                "приголосну букву.\n");

        StringBuilder builder = new StringBuilder();
        Pattern pattern = Pattern.compile("\\b[йцкнгшщзхфвпрлджчсмтьбю].{" + (wordLength - 1) + "}?\\b");
        Matcher matcher = pattern.matcher(text);
        int previousEnd = 0;
        while (matcher.find()) {
            if (previousEnd != 0)
                builder.append(text, previousEnd, matcher.start());
            else
                builder.append(text, 0, matcher.start());
            previousEnd = matcher.end();
        }
        builder.append(text.substring(previousEnd));
        System.out.println(builder.toString());
    }

    /**
     * Sorts and prints words which has a specified letter in descending order.
     * Words which has same count of this letter are sorted in alphabet order.
     *
     * @param text string to handle.
     */
    void task13(String text) {
        System.out.println("Відсортувати слова у тексті за спаданням кількості входжень заданого\n" +
                "символу, а у випадку рівності - за алфавітом.\n");

        Pattern pattern = Pattern.compile(String.format("[%c]", character));

        String[] split = text.replaceAll("^[.,\\s():]+", "").split("[.,\\s():]+");
        List<String> list = Arrays.asList(split);

        list.sort((o1, o2) -> {
            String _o1 = o1.toLowerCase();
            String _o2 = o2.toLowerCase();
            Matcher matcher1 = pattern.matcher(_o1);
            Matcher matcher2 = pattern.matcher(_o2);

            int count1 = 0;
            int count2 = 0;
            while (matcher1.find()) {
                count1++;
            }
            while (matcher2.find()) {
                count2++;
            }
            if (count1 == count2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o2, o1);
            }
            return count2 - count1;
        });

        list.forEach(System.out::println);
    }

    void task14(String text) {
        System.out.println("У заданому тексті знайти підрядок максимальної довжини, який є\n" +
                "паліндромом, тобто, читається зліва на право і справа на ліво однаково.\n");

        // TODO:
    }

    /**
     * Erases all entries of first letter in each word except first letter.
     *
     * @param text string to handle.
     */
    void task15(String text) {
        System.out.println("Перетворити кожне слово у тексті, видаливши з нього всі наступні\n" +
                "(попередні) входження першої (останньої) букви цього слова.\n");

        String[] split = text.replaceAll("^[.,\\s():]+", "").split("[.,\\s():]+");
        StringBuilder builder = new StringBuilder();

        for (String s : split) {
            builder.append(removeAllLettersLikeFirstLettes(s)).append(" ");
        }

        System.out.println(builder);
    }

    /**
     * In each senctence replace all the words of given length <code>wordLength</code> with another given substring.
     *
     * @param text string to handle.
     */
    void task16(String text) {
        System.out.println("У певному реченні тексту слова заданої довжини замінити вказаним\n" +
                "підрядком, довжина якого може не співпадати з довжиною слова.\n");

        StringBuilder builder = new StringBuilder();
        Pattern pattern = Pattern.compile("\\b[йцкнгшщзхфвпрлджчсмтьбю].{" + (wordLength - 1) + "}?\\b");
        Matcher matcher = pattern.matcher(text.toLowerCase());

        int previousEnd = 0;
        while (matcher.find()) {
            if (previousEnd != 0)
                builder.append(text, previousEnd, matcher.start());
            else
                builder.append(text, 0, matcher.start());
            previousEnd = matcher.end();
            builder.append(substring);
        }
        builder.append(text.substring(previousEnd));
        System.out.println(builder.toString());
    }

    String removeAllLettersLikeFirstLettes(String word) {
        String firstLetter = new String(new char[]{word.charAt(0)});
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 1; i < word.toCharArray().length; i++) {
            if (word.toLowerCase().charAt(i) != firstLetter.toLowerCase().charAt(0)) {
                stringBuilder.append(word.charAt(i));
            }
        }
        return stringBuilder.toString();
    }


}

class Cl {
    public static void main(String[] args) {
        new Main().task12("лоав квыпвасисмиі рпракі. тоатмрактмлд аатлил апв лі аптвапвап. фіавік ваіів");
    }
}